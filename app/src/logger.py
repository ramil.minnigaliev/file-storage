import logging
import logging.handlers
import logging.config

LOG_LEVEL_DEFAULT = 'DEBUG'
LOG_FORMAT_DEFAULT = '[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s'
LOG_ROTATE_SIZE_DEFAULT = 0
LOG_ROTATE_COUNT_DEFAULT = 1


def init_logger(config: dict = None, reset_handlers: bool = True) -> None:
    """Init root logger from config."""
    log_level = LOG_LEVEL_DEFAULT
    log_format = LOG_FORMAT_DEFAULT
    log_rotate_size = LOG_ROTATE_SIZE_DEFAULT
    log_rotate_count = LOG_ROTATE_COUNT_DEFAULT
    log_filename = None
    if config:
        if 'level' in config:
            log_level = config['level']
        if 'format' in config:
            log_format = config['format']
        if 'rotate_size' in config:
            log_rotate_size = config['rotate_size']
        if 'rotate_count' in config:
            log_rotate_count = config['rotate_count']
        if 'filename' in config:
            log_filename = config['filename']

    logger = logging.getLogger()
    logger.setLevel(log_level)
    formatter = logging.Formatter(log_format)
    if log_filename:
        handler = logging.handlers.RotatingFileHandler(
            filename=log_filename,
            maxBytes=int(float(log_rotate_size) * 1024 * 1024),
            backupCount=int(log_rotate_count)
        )
    else:
        handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    if reset_handlers:
        logger.handlers.clear()
    logger.addHandler(handler)
