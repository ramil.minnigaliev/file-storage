from os import getenv

config = {
    'app': {
        'storage_path': getenv('STORAGE_PATH', 'files'),
        'download_url': getenv('DOWNLOAD_URL', 'http://localhost:8080/download')
    },
    'logging': {
        'filename': getenv('LOG_FILENAME')
    }
}
