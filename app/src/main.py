import logging

from aiohttp import web

from config import config
from logger import init_logger
from views import upload


init_logger(config['logging'])
logger = logging.getLogger()


if __name__ == '__main__':
    logger.info('Server starting...')

    app = web.Application()
    app.router.add_post('/upload', upload)

    web.run_app(app)
