import logging
import os

from aiohttp import web

from config import config
from utils import generate_filename


logger = logging.getLogger()


async def upload(request):
    try:
        reader = await request.multipart()
        field = await reader.next()
        assert field.name == 'file'
        filename = generate_filename(field.filename)
        filepath = os.path.join(config['app']['storage_path'], filename)
        size = 0
        logger.info(f"Uploading {field.filename} to {filepath}.")
        with open(filepath, 'wb') as f:
            while True:
                chunk = await field.read_chunk()  # 8192 bytes by default.
                if not chunk:
                    break
                size += len(chunk)
                f.write(chunk)
        logger.info(f"Uploaded successfully {field.filename} to {filepath}. Size: {round(size / 10e6, 3)}M")
        return web.json_response({
            'url': f"{config['app']['download_url']}/{filename}",
            'size': size
        })
    except BaseException as e:
        logger.exception(repr(e))
