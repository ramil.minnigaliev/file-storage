import os
from hashlib import md5
from time import time


def generate_filename(original_filename: str = '') -> str:
    """Generate unique filename."""
    filename, file_extension = os.path.splitext(original_filename)
    unique_str = f"{filename}-{time()}"
    return md5(unique_str.encode()).hexdigest() + file_extension
