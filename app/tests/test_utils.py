from utils import generate_filename


def test_generate_filename():
    filename = generate_filename()
    assert len(filename) == 32
    assert filename != generate_filename()
    assert generate_filename('file.ext').split('.')[-1:][0] == 'ext'
