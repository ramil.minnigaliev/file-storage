import aiohttp
import pytest
from aiohttp import web

from views import upload


@pytest.fixture
def cli(loop, aiohttp_client):
    app = web.Application()
    app.router.add_post('/upload', upload)
    return loop.run_until_complete(aiohttp_client(app))


async def test_upload(cli):
    data = 'Some data'

    form = aiohttp.FormData()
    form.add_field('file', data, filename='file.txt')
    resp = await cli.post('/upload', data=form)

    assert resp.status == 200
    resp_json = await resp.json()
    assert 'url' in resp_json
    assert 'size' in resp_json
    assert resp_json['size'] == len(data)
