FROM python:3.8-slim
LABEL name="file-storage"
COPY ./ /app
RUN \
    apt-get update && \
    apt-get install --no-install-recommends -y gcc && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
RUN \
    pip install --no-cache-dir -r /app/requirements/requirements.in && \
    pip install --no-cache-dir -r /app/requirements/requirements.dev.in
WORKDIR /app/src
ENTRYPOINT ["python", "main.py"]
EXPOSE 8080
